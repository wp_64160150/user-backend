import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductsDto } from './dto/create-products.dto';
import { UpdateProductsDto } from './dto/update-products.dto';
import { Products } from './entities/products.entity';
let products: Products[] = [
  { id: 1, name: 'Milk', price: 40 },
  { id: 2, name: 'Milo', price: 50 },
  { id: 3, name: 'Ovaltine', price: 55 },
];
let lastProductsId = 4;
@Injectable()
export class ProductsService {
  create(CreateProductsDto: CreateProductsDto) {
    const newProducts: Products = {
      id: lastProductsId++,
      ...CreateProductsDto, // name,price
    };
    products.push(newProducts);
    return newProducts;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, UpdateProductsDto: UpdateProductsDto) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProducts: Products = {
      ...products[index],
      ...UpdateProductsDto,
    };
    products[index] = updateProducts;
    return updateProducts;
  }

  remove(id: number) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedUser = products[index];
    products.splice(index, 1);
    return deletedUser;
  }
  reset() {
    products = [
      { id: 1, name: 'Milk', price: 40 },
      { id: 2, name: 'Milo', price: 50 },
      { id: 3, name: 'Ovaltine', price: 55 },
    ];
    lastProductsId = 4;
    return 'RESET';
  }
}
