import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';
export class CreateProductsDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string; // 8 char

  @IsNotEmpty()
  @IsPositive()
  price: number; //
}
